package com.wipro.catalog;

public class ProductNotFoundException extends RuntimeException {

    private final Integer id;

    public ProductNotFoundException(final Integer id) {
        super("Product not found");
        this.id = id;
    }

    public Integer getId() {
        return id;
    }
}
