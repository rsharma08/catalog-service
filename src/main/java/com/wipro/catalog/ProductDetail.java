package com.wipro.catalog;

import java.util.List;

public class ProductDetail  extends Product{
    private List<SKU> skus;

    public ProductDetail(Product product) {
        super();
        setId(product.getId());
        setName(product.getName());
        setCatalog(product.getCatalog());
    }

    public List<SKU> getSkus() {
        return skus;
    }

    public void setSkus(List<SKU> skus) {
        this.skus = skus;
    }
}
